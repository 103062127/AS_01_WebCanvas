﻿var actionsLogger = {
    kSequenceStartString: "/*s*/\r\n",
    kSequenceStartStringForSearch: "/*s*/",
    _UndoStack: [],

    addScriptWithUndoSupport: function(script, shouldUndo) {
        var console = document.getElementById('ctl00_ContentPlaceHolder1_hfGeneratedScriptContent');

        if (console != null) {
            var undoText = "";
            if (shouldUndo) {
                undoText = this.kSequenceStartString;

                UiManager.Enable("undo");
                UiManager.Disable("redo");
                this._UndoStack = [];
            }

            console.value += undoText + script;
        }


        //var console = document.getElementById('scriptContent');
        //if (console != null) {
        //    console.innerHTML = this._actionsScript.replace("\r\n", "<br/>");
        //}
    },

    addScript: function(script) {
        var retVal = this.addScriptWithUndoSupport(script, true);

        //UiManager.ButtonFlash();
        UiManager.AllowSubmit();


        return retVal;
    },

    addScriptWithNoUndo: function(script) {
        return this.addScriptWithUndoSupport(script, false);
    },

    startSequence: function() {
        this.addScript(this.kSequenceStartString);
    },

    isLoggerStarted: function() {
        var lines = this.getScript().split("\n");
        return (lines.length > 5);
    },

    getScript: function() {
        var console = document.getElementById('ctl00_ContentPlaceHolder1_hfGeneratedScriptContent');
        if (console != null) {
            return console.value;
        }
        return "";
    },

    setScript: function(script) {
        var console = document.getElementById('ctl00_ContentPlaceHolder1_hfGeneratedScriptContent');
        if (console != null) {
            console.value = script;
        }
    },

    Undo: function() {
        var scriptContent = this.getScript();
        var lastSequenceIndex = scriptContent.lastIndexOf(this.kSequenceStartStringForSearch);

        if (lastSequenceIndex < 0) { //no last action available
            UiManager.Disable("undo");
        } else {
            var undoScript = scriptContent.slice(lastSequenceIndex, scriptContent.length);
            scriptContent = scriptContent.slice(0, lastSequenceIndex);
            paintManager.RepaintByScript(scriptContent);
            this.setScript(scriptContent);

            this._UndoStack.push(undoScript);

            UiManager.Enable("redo");
        }
    },

    Redo: function() {
        var redoScript = this._UndoStack.pop();

        if (redoScript) {
            var scriptContent = this.getScript();
            scriptContent = scriptContent + redoScript;
            paintManager.RepaintByScript(scriptContent);
            this.setScript(scriptContent);
        } else {
            UiManager.Disable("redo");
        }
    }

};